// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Extensions",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(name: "Extensions", targets: ["Extensions"]),
        .library(name: "CombineExtensions", targets: ["CombineExtensions"]),
    ],
    targets: [
        .target(
            name: "Extensions",
            dependencies: [
                "CombineExtensions",
                "SwiftExtensions"
            ]
        ),
        .target(
            name: "CombineExtensions"
        ),
        .target(
            name: "SwiftExtensions"
        ),
        .testTarget(
            name: "ExtensionsTests",
            dependencies: ["Extensions"]),
    ]
)
