// Copyright © Blockdaemon All rights reserved.

import CryptoKit

protocol GenericPasswordConvertible: CustomStringConvertible {
    init<D>(rawRepresentation data: D) throws where D: ContiguousBytes
    var rawRepresentation: Data { get }
}

extension SymmetricKey: GenericPasswordConvertible {
    public var description: String { "symmetrically" }
    public init<D>(rawRepresentation data: D) throws where D: ContiguousBytes { self.init(data: data) }
    public var rawRepresentation: Data { withUnsafeBytes { Data($0) } }
}

extension OSStatus {
    public var secErrorMessage: String {
        (SecCopyErrorMessageString(self, nil) as String?) ?? "\(self)"
    }
}
