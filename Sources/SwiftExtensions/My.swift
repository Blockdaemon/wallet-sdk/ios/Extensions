// Copyright © Blockdaemon All rights reserved.
// swiftlint:disable type_name

extension CustomStringConvertible {
    public typealias My = Self
}

extension Equatable {
    public typealias My = Self
}

extension RawRepresentable {
    public typealias My = Self
}

extension Sequence {
    public typealias My = Self
}

extension Decodable {
    public typealias My = Self
}

extension Encodable {
    public typealias My = Self
}
